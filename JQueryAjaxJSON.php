<?php
//traitement requête API, envoie des données au client

header("Content-type: application/json");

function anneePropre($date)
{
    if (mb_strlen($date)>4){
        $date=substr($date, 0, 4);
    }
    return $date;
}

if (!empty($_REQUEST['titre'])) {
    $titre= $_REQUEST['titre'];
    $apiKey="83fce3b4";
    $apiUrl="http://www.omdbapi.com/?apikey=". $apiKey . "&s=" . $titre;
    $response=file_get_contents($apiUrl, false);
    $data=json_decode($response, true);

    $films=[];


    foreach ($data['Search'] as $film) {
        $films[]=[
            'titre'=>$film['Title'],
            'annee'=>anneePropre($film['Year']),
            'img'=>$film['Poster']
        ];
    }
    
    echo json_encode($films,JSON_PRETTY_PRINT);
}
